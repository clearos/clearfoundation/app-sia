<?php

$lang['sia_app_name'] = 'Sia Manager';
$lang['sia_app_description'] = 'Sia is the leading decentralized cloud storage platform.';

$lang['sia_app_primary_seed'] = 'Primary seed';
$lang['sia_app_seeds'] = 'Seeds';

$lang['sia_app_seed_table_title'] = 'List of seeds in use by the wallet';
$lang['sia_app_return_summary'] = 'Return to Summary';

$lang['sia_app_announce_host'] = 'Announce your host to the network.';
$lang['sia_app_announce_host_address'] = 'Address to announce(netaddress)';
$lang['sia_app_announce_submit'] = 'Announce';


$lang['sia_app_folder_title'] = 'Storage Folder';
$lang['sia_app_folder_path'] = 'Folder path';
$lang['sia_app_capacity_folder'] = 'Capacity of the storage folder(GB)';
$lang['sia_app_folder_submit'] = 'Resize';
$lang['sia_app_add_folder_submit'] = 'Add';

//summry
$lang['sia_app_help'] = "<span id='storage_statuss'>To start hosting: Add a storage folder,
Set your prefered price, bandwidth cost, collateral, and duration.
Set 'Accepting Contracts' to 'Yes'
Announce your host by clicking the  'Announce' button.</span>";
$headers = array('Confirmed balance','Fund balance','Wallet unlocked','Block Height');

$lang['sia_app_confirmed'] = 'Confirmed balance';
$lang['sia_app_balance'] = 'Fund balance';
$lang['sia_app_unlocked'] = 'Wallet unlocked';
$lang['sia_app_height'] = 'Block Height';
$lang['sia_warning_seeds'] = 'Please keep these seed keys in a safe place in order to  unlock your wallet.';


$lang['sia_app_my_account'] = 'My Account';
$lang['sia_app_my_sia_funds'] = 'My Sia funds';
$lang['sia_app_wallet_lock'] = 'Wallet Lock';
$lang['sia_app_announce'] = 'Announce Host';
$lang['sia_app_unlock'] = 'Wallet Unlock';
$lang['sia_app_create_account'] = 'Create Account';


$lang['sia_app_buy_sia'] = 'Buy Sia';
$lang['sia_app_send_sia'] = 'Send Sia';

$lang['sia_app_wallet_address_title'] = 'Wallet addresses';
$lang['sia_app_wallet_address'] = 'Wallet address';
$lang['sia_app_wallet_create_addresses'] = 'Create new address';

$headers = array('Capacity','Capacity remaining','Path','Progress Numerator');
$tb_title = 'Hosts Storage Manager';

$lang['sia_app_capacity'] = 'Capacity';
$lang['sia_app_add_capacity_remaining'] = 'Capacity remaining';
$lang['sia_app_path'] = 'Path';
$lang['sia_app_progress'] = 'Progress Numerator';
$lang['sia_app_hosts_manager'] = 'Hosts Storage Manager';
$lang['sia_app_add_folder'] = 'Add Folder';
$lang['sia_app_folder_resize'] = 'Resize';
$lang['sia_app_folder_delete'] = 'Delete';

$lang['sia_app_host_settings'] = 'Hosting Settings';
$lang['sia_app_max_duration'] = 'Max Duration (Weeks)';
$lang['sia_app_collateral'] = 'Collateral per TB per Month (SC)';
$lang['sia_app_price'] = 'Price per TB per Month (SC)';
$lang['sia_app_folder_bandwidth'] = 'Bandwidth Price (SC/TB)';
$lang['sia_app_folder_Accepting'] = 'Accepting Contracts';
$lang['sia_app_folder_save'] = 'Save';

$lang['sia_funds_title'] = 'Send Sia Funds';
$lang['sia_siafunds'] = 'Number of siafunds';
$lang['sia_destination'] = 'Address that is receiving the funds';
$lang['sia_send'] = 'Send';

$lang['sia_password_title'] = 'Change Password';
$lang['sia_current_password'] = 'Current Password';
$lang['sia_new_password'] = 'New Password';
$lang['sia_re_new_password'] = 'New Password again';
$lang['sia_change_password_submit'] = 'Change';

$lang['sia_change_password_anchor'] = 'Change Password';
$lang['sia_seed_anchor'] = 'Keys';


$lang['sia_initializes_password'] = 'Encryption Password';
$lang['sia_re_initializes_password'] = 'Encryption Password again';
$lang['sia_initializes_title'] = 'Password that will be used to encrypt the wallet';
$lang['sia_initializes_button'] = 'Initializes the wallet';
$lang['password_is_too_short'] = 'Encryption Password is too short';
$lang['password_confirmation'] = ' Your Encryption password and confirmation Encryption password do not match.';

$lang['sia_contract_title'] = 'Contract Information';
$lang['sia_contract_host'] = 'Host Status';
$lang['sia_contract_active'] = 'Active Contract';
$lang['sia_contract_earned'] = 'Earned';
$lang['sia_contract_expected'] = 'Expected';


$lang['sia_unlock_title'] = 'Wallet Unlock';
$lang['sia_unlock_password'] = 'Encryption Password';
$lang['sia_unlock_button'] = 'Wallet Unlock';
$lang['sia_app_connection_error'] = "Please start Sia by clicking the 'Start' button.";
$lang['sia_app_wallet_unlocked'] = "Wallet must be unlocked before it can be used";
$lang['sia_app_404_refer_api'] = "404 - Refer to API.md";
$lang['sia_app_must_absolute'] = "Storage folder paths must be absolute.";
$lang['sia_app_no_such_file'] = "No such file or directory.";
$lang['sia_app_path_is_already'] = "Selected path is already in use as a storage folder";
$lang['sia_app_allowed_size'] = "Minimum allowed size for a storage folder is 268435456 bytes";
$lang['sia_app_authentication'] = "API authentication failed.";
$lang['sia_app_key_is_incorrect'] = "The provided encryption key is incorrect.";
$lang['sia_app_folder_exists'] = "No storage folder exists at that index!";
$lang['sia_app_not_find_storage'] = "Could not find storage folder with that id.";
$lang['sia_app_encrypt_again'] = "Wallet is already encrypted, cannot encrypt again.";
$lang['sia_app_local_net_address'] = "Unable to perform manual host announcement: announcement requested with local net address.";
$lang['sia_app_port_in_address'] = "Announcement requested with bad net address and missing port in address";
$lang['sia_app_wallet_is_locked'] = "Cannot announce the host while the wallet is locked";
$lang['sia_app_insufficient'] = "Insufficient balance.";
$lang['sia_app_password_must'] = "A password must be provided!";



