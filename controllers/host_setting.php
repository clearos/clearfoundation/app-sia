<?php

/**
 * Hosting Setting controller.
 *
 * @category   apps
 * @package    sia
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2012-2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/sia/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

// Exceptions
//-----------

use \clearos\apps\base\Engine_Exception as Engine_Exception;

clearos_load_library('base/Engine_Exception');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Host Setting controller.
 *
 * @category   apps
 * @package    sia
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2012-2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/sia/
 */

class Host_Setting extends ClearOS_Controller
{
    /**
     * Host Setting summary view.
     *
     * @return view
     */

    function index()
    {
        $this->_page('settings');
    }
        
    /**
    * View/edit common view
    *
    * @return view
    */

    function announce()
    {
        $this->_page_announce('announce');
    }

    /**
    * Announce the host to the network as a source of storage.
    *
    * @param @string $type for view
    *
    * @return View
    */

    function _page_announce($type = "announce")
    {
        // Load libraries
        //---------------

        $this->load->library('sia/Sia');

        $data['hostingsetting'] = $this->sia->get_hosting_settings();

        if ($_POST) {

            $this->form_validation->set_policy('address_announce', 'sia/Sia', 'validate_host', TRUE);

            $form_ok = $this->form_validation->run();

            if ($form_ok) {

                try {
                    $address_announce  = $this->input->post('address_announce');

                    $this->sia->host_announce($address_announce);
                    
                    redirect('/sia');

                } catch (Exception $e) {
                    $this->page->view_exception($e);
                    return;
                }
            }
        }
            
        $this->page->view_form('host_setting/host_announce', $data);
    }

    /**
    * Configures hosting parameters.
    *    
    * @param @string $type for view mode
    *
    * @return @array Array of available hosting details
    */

    function _page($type = "settings")
    {
        // Load libraries
        //---------------

        $this->load->library('sia/Sia');
        $this->lang->load('sia');

        if ($_POST) {

            // Handle Form 
            //------------------

            $this->form_validation->set_policy('max_duration', 'sia/Sia', 'validate_integer_value', TRUE);
            $this->form_validation->set_policy('collateral', 'sia/Sia', 'validate_integer_value', TRUE);
            $this->form_validation->set_policy('price_per_tb', 'sia/Sia', 'validate_integer_value', TRUE);
            $this->form_validation->set_policy('bandwidth', 'sia/Sia', 'validate_integer_value', TRUE);

            $form_ok = $this->form_validation->run();

            if ($form_ok) {

                $max_duration = $this->input->post('max_duration');
                $collateral = $this->input->post('collateral');
                $price_per_tb = $this->input->post('price_per_tb');
                $bandwidth = $this->input->post('bandwidth');
                $accepting = $this->input->post('accepting');
                
                if ($accepting) {
                    $accepting ='true';
                } else {
                    $accepting ='false';
                }

                try {

                    $this->sia->set_hosting_settings($max_duration, $collateral, $price_per_tb, $bandwidth, $accepting);
                    redirect('/sia');

                } catch (Exception $e) {

                    $this->page->view_exception($e);
                    return;
                }
            }
        }
        
        try {
            $data['hostingsetting'] = $this->sia->get_hosting_settings();

            $this->page->view_form('host_setting/summary', $data);

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }
    }
}