<?php
/**
 * Wallet controller.
 *
 * @category   apps
 * @package    sia
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2012-2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/sia/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

// Exceptions
//-----------

use \clearos\apps\base\Engine_Exception as Engine_Exception;
use \clearos\apps\base\File_Not_Found_Exception as File_Not_Found_Exception;

clearos_load_library('base/File_Not_Found_Exception');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Wallet controller.
 *
 * @category   apps
 * @package    sia
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2012-2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/sia/
 */

class Wallet extends ClearOS_Controller
{
    /**
     * Wallet summary view.
     *
     * @return view
     */

    function index()
    {
        // Load libraries
        //---------------
    
        try {
           $this->load->library('sia/Sia');

        } catch (Exception $e) {
            // nothing to do right now but soon we need to handle it

        }    
        
        $this->lang->load('sia');
        
        try {
            $block = $this->sia->get_wallet_details();
            $wallet_lock_status = $this->sia->wallet_lock_status();
            $user_unlocked = $this->sia->check_user_unlock_exists();
            $wallet_unlocked = $wallet_lock_status;

            $data = array();

            $data['block'] = $block;
            $data['wallet_address'] = $this->sia->get_wallet_address();
            $data['wallet_seeds'] = $this->sia->get_wallet_seeds();
            $data['wallet_lock_status'] = $wallet_lock_status;
            $data['show_loader'] = FALSE;

            if (!$wallet_unlocked && $user_unlocked && $block['encrypted']) {
                $data['show_loader'] = TRUE;
            }
            
            $this->page->view_form('wallet/summary', $data);

        } catch (Exception $e) {
            return FALSE;
        }    
    }

    /**
    * get the list of seeds in use by the wallet. 
    *
    * @return view
    */

    function get_seeds()
    {
        // Load libraries
        //---------------

        $this->load->library('sia/Sia');

        try {
            $data['seeds'] = $this->sia->get_wallet_seeds();
            $this->page->view_form('wallet/seeds', $data);

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }
    }

    /**
     * Wallet lock.
     *
     * @return redirect
     */

    function wallet_lock()
    {
        // Load libraries
        //---------------
        $this->load->library('sia/Sia');
        
        try {
            $this->sia->wallet_lock();
            redirect('/sia');

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }
    }

    /**
    * Wallet unlock
    *
    * @return redirect
    */

    function wallet_unlock()
    {
        // Load libraries
        //---------------

        $this->load->library('sia/Sia');

        if ($_POST) {

            $this->form_validation->set_policy('password', 'sia/Sia', 'validate_password', TRUE);

            $form_ok = $this->form_validation->run();

            if ($form_ok) {

                $password = $this->input->post('password');

                try {
                    $this->sia->wallet_unlock($password);
                    redirect('/sia');

                } catch (Exception $e) {
                    $this->page->view_exception($e);
                    return;
                }
            }
        }

        $this->page->view_form('wallet/wallet_unlock');
    }

    /**
    * Generate a new address for the same wallet using primary seeds.
    *
    * @return redirect 
    */

    function get_address()
    {
        // Load libraries
        //---------------

        $this->load->library('sia/Sia');

        try {
            $this->sia->create_wallet_address();
            redirect('/sia');

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }
    }

    /**
    * Initializes the wallet.
    *
    * @return View
    */

    function initializes()
    {
        // Load libraries
        //---------------

        $this->load->library('sia/Sia');

        if ($_POST) {

            $this->form_validation->set_policy('password', 'sia/Sia', 'validate_password', TRUE);
            $this->form_validation->set_policy('repassword', 'sia/Sia', 'validate_password', TRUE);

            $form_ok = $this->form_validation->run();

            $password = $this->input->post('password');
            $repassword = $this->input->post('repassword');

            if ($password !== $repassword) {
                $this->form_validation->set_error('repassword', lang('password_confirmation'));

                $form_ok = FALSE;
            }

            if ($form_ok) {

                $password = $this->input->post('password');

                try {
                    $data['seeds'] = $this->sia->wallet_initializing($password);

                    $this->page->view_form('wallet/create_seeds', $data);
                    return;

                } catch (Exception $e) {
                    $this->page->view_exception($e);
                    return;
                }
            }
        }

        $this->page->view_form('wallet/initializes_password'); 
    }

    /**
    * Checked wallet status lock and unlock.
    *
    * @return @boolean of wallet status.
    */

    function get_wallet_status()
    {
        // Load libraries
        //---------------

        $this->load->library('sia/Sia');
        
        try {
            $wallet_unlocked = $this->sia->wallet_lock_status();
            $user_unlocked = $this->sia->check_user_unlock_exists();

            if ($wallet_unlocked && $user_unlocked) {
                $data = TRUE;
            }

        } catch (Exception $e) {
            $data = clearos_exception_code($e);
            
        }

        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Fri, 01 Jan 2010 05:00:00 GMT');
        header('Content-type: application/json');

        $this->output->set_output(json_encode($data));
    }

    /**
    * Sends siafunds to an addres.
    *
    * @return redirect.
    */

    function siafunds()
    {
        // Load libraries
        //---------------

        $this->load->library('sia/Sia');

        if ($_POST) {

            $this->form_validation->set_policy('sia_siafunds', 'sia/Sia', 'validate_siafunds', TRUE);
            $this->form_validation->set_policy('sia_destination', 'sia/Sia', 'validate_siafunds', TRUE);

            $form_ok = $this->form_validation->run();


            if ($form_ok) {

                $sia_siafunds = $this->input->post('sia_siafunds');
                $sia_destination = $this->input->post('sia_destination');

                try {
                    $this->sia->send_sia_funds($sia_siafunds, $sia_destination);

                    redirect('/sia');

                } catch (Exception $e) {
                    $this->page->view_exception($e);
                    return;
                }
            }
        }
            
        $this->page->view_form('wallet/sia_funds'); 
    }

    /**
    * Changes the wallet's encryption key.
    *
    * @return redirect.
    */

    function change_password()
    {

        // Load libraries
        //---------------

        $this->load->library('sia/Sia');

        if ($_POST) {

            $this->form_validation->set_policy('encryptionpassword', 'sia/Sia', 'validate_encryptionpassword', TRUE);
            $this->form_validation->set_policy('password', 'sia/Sia', 'validate_password', TRUE);
            $this->form_validation->set_policy('repassword', 'sia/Sia', 'validate_password', TRUE);

            $form_ok = $this->form_validation->run();

            $newpassword = $this->input->post('password');
            $repassword = $this->input->post('repassword');

            if ($newpassword !== $repassword) {
                $this->form_validation->set_error('repassword', lang('password_confirmation'));

                $form_ok = FALSE;
            }

            if ($form_ok) {

                $encryptionpassword = $this->input->post('encryptionpassword');
                $newpassword = $this->input->post('password');

                try {
                    $this->sia->change_wallet_password($encryptionpassword, $newpassword);

                    redirect('/sia');

                } catch (Exception $e) {
                    $this->page->view_exception($e);
                    return;
                } 
            }    
        } 
            
        $this->page->view_form('wallet/change_password');       
    }
}