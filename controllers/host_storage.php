<?php

/**
 * Host Storage controller.
 *
 * @category   apps
 * @package    sia
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2012-2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/sia/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

// Exceptions
//-----------

use \clearos\apps\base\Engine_Exception as Engine_Exception;

clearos_load_library('base/Engine_Exception');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Host Storage controller.
 *
 * @category   apps
 * @package    sia
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2012-2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/sia/
 */

class Host_Storage extends ClearOS_Controller
{
    /**
     * Host Storage summary view.
     *
     * @return view
     */

    function index()
    {
        // Load libraries
        //---------------

        $this->load->library('sia/Sia');
        $this->lang->load('sia');
        
        try {
            $data['host_storage'] = $this->sia->get_host_storage_info();
            $data['wallet_lock_status'] = $this->sia->wallet_lock_status();
            $this->page->view_form('host_storage/summary', $data);

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }    
    }

    /**
    * Get listing.
    *
    * @return @array folder list.
    */

    function get_listing()
    {
        $path = $this->input->get('path');

        $this->load->library('sia/Host_storage');
        $this->lang->load('sia');
        
        try {
            $response = $this->host_storage->get_folder_list($path);

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        } 
    }
    
    /**
    * Adds a storage folder to the manager.
    *  
    * @return View
    */

    function add_folder()
    {
        // Load libraries
        //---------------

        $this->load->library('sia/Sia');

        if ($_POST) {

            $this->form_validation->set_policy('storage_path', 'sia/Sia', 'validate_folder_size', TRUE);
            $this->form_validation->set_policy('storage_folder_size', 'sia/Sia', 'validate_storage_path', TRUE);

            $form_ok = $this->form_validation->run();

            if ($form_ok) {

                $storage_path = $this->input->post('storage_path');
                $storage_folder_size = $this->input->post('storage_folder_size');
                
                try {
                    $this->sia->add_storage_folder($storage_path, $storage_folder_size);
                    redirect('/sia');

                } catch (Exception $e) {
                    $this->page->view_exception($e);
                    return;
                } 
            }
        }

        $this->page->view_form('host_storage/folder');
    }

    /**
    * Remove a storage folder from the manager.
    *
    * @param $string $hostpath of folder
    *
    * @return Redirect
    */

    function remove_folder($hostpath)
    {
        // Load libraries
        //---------------

        $this->load->library('sia/Sia');

        $hostpath = base64_decode(strtr($hostpath, '-_.', '+/='));

        try {
            $this->sia->remove_host_folder($hostpath);
            redirect('/sia');

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }
    }

    /**
    * Grows or shrinks a storage file in the manager.
    *
    * @param $string $hostpath of folder
    *
    * @return View
    */

    function resize_folder($hostpath)
    {
        // Load libraries
        //---------------

        $this->load->library('sia/Sia');

        // decode host path
        $data['hostpath'] = base64_decode(strtr($hostpath, '-_.', '+/='));
        $data['encode_value'] = $hostpath;

        if ($_POST) {

            $this->form_validation->set_policy('storage_folder_size', 'sia/Sia', 'validate_folder_size', TRUE);
            $this->form_validation->set_policy('storage_path', 'sia/Sia', 'validate_storage_path', TRUE);

            $form_ok = $this->form_validation->run();

            if ($form_ok) {

                $storage_path = $this->input->post('storage_path');
                $storage_folder_size = $this->input->post('storage_folder_size');

                try {
                    $this->sia->resize_host_folder($storage_path, $storage_folder_size);

                    redirect('/sia');

                } catch (Exception $e) {

                    $this->page->view_exception($e);
                    return;
                } 
            }
        }

        $this->page->view_form('host_storage/folder_resize', $data); 
    }
}