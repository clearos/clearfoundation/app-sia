<?php 

/**
 * Sia class.
 *
 * Sia is the leading decentralized cloud storage platform.
 *
 * @category   apps
 * @package    sia
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/sia/
 */
///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\sia;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('sia');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

use \clearos\apps\base\Engine as Engine;
use \clearos\apps\base\File as File;
use \clearos\apps\base\Shell as Shell;
use \clearos\apps\base\Folder as Folder;
use \clearos\apps\storage\Storage as Storage;

use \Exception as Exception;
use \clearos\apps\base\Engine_Exception as Engine_Exception;
use \clearos\apps\base\Validation_Exception as Validation_Exception;
use \clearos\apps\base\File_Not_Found_Exception as File_Not_Found_Exception;

clearos_load_library('base/Engine');
clearos_load_library('base/Folder');
clearos_load_library('base/File');
clearos_load_library('base/Shell');
clearos_load_library('storage/Storage');

clearos_load_library('base/Engine_Exception');
clearos_load_library('base/Validation_Exception');
clearos_load_library('base/File_Not_Found_Exception');
///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Sia class.
 *
 * @category   apps
 * @package    sia
 * @subpackage libraries
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2019 ClearCenter
 * @license    http://www.clearcenter.com/app_license ClearCenter license
 * @link       http://www.clearcenter.com/support/documentation/clearos/clearglass-community/
 */

class Sia extends Engine
{

    ///////////////////////////////////////////////////////////////////////////////
    // C O N S T A N T S
    ///////////////////////////////////////////////////////////////////////////////
    

    const COMMAND_CURL = '/usr/bin/curl';
    const COMMAND_LOCATE = '/usr/bin/find';
    const HOST = '127.0.0.1:9980';
    const FOLDER_API = "/var/lib/docker/overlay2";
    const FILE_USER_UNLOCK = '/var/clearos/sia/wallet_unlocked.lock';
    const WALLET_SETTINGS_FILE = '/var/clearos/sia/wallet-settings.backup';
    const MIN_PASSWORD_LENGTH = 5;

    ///////////////////////////////////////////////////////////////////////////////
    // M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Sia constructor.
     */

    public function __construct()
    {
        clearos_profile(__METHOD__, __LINE__);
        
        try {
            $this->apipassword = $this->_get_sia_apipassword();
            $this->common_cmd_call = '-A "Sia-Agent" --user "":"'.$this->apipassword.'"';
        } catch (Exception $e) {
            // No need to do anything
        }
    }

    /**
     * Get status information about the host.
     *
     * @return @array Array of available host details
     */

    public function host_details()
    {
        clearos_profile(__METHOD__, __LINE__);

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();
        $retval = $shell->execute(self::COMMAND_CURL, $this->common_cmd_call.' "'.self::HOST.'/host"', TRUE, $options);
        $lines = $shell->get_output();

        return $this->_filter_response($lines, 3);
    }

    /**
     *  Basic information about the wallet,
     *
     * @return @array Array of available Wallet details
     */

    public function get_wallet_details()
    {
        clearos_profile(__METHOD__, __LINE__);

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();
        $retval = $shell->execute(self::COMMAND_CURL, $this->common_cmd_call.' "'.self::HOST.'/wallet"', TRUE, $options);
        $output = $shell->get_output();
        $error = (preg_match('/Failed connect/', $output[2])) ? lang('sia_app_connection_error') : NULL;
        
        if ($error) {
            throw new Engine_Exception($error);
        
        } else {
            return $this->_filter_response($output, 3);
        }
    }

    /**
     * Checked  Wallet status for lock and unlock
     *
     * @return @boolean True and False
     */

    public function wallet_lock_status()
    {
        clearos_profile(__METHOD__, __LINE__);

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();
        $retval = $shell->execute(self::COMMAND_CURL, $this->common_cmd_call.' "'.self::HOST.'/wallet"', TRUE, $options);
        $output = $shell->get_output();
        $error = (preg_match('/Failed connect/', $output[2])) ? lang('sia_app_connection_error') : NULL;
        
        if ($error) {
            throw new Engine_Exception($error);
        
        } else {
            $data = $this->_filter_response($output, 3);
            $status = $data['unlocked'];

            return $status;
        }
    }

    /**
     * Gets a list of folders tracked by the host's storage manager.
     *
     * @return @array Array of available folders
     */

    public function get_host_storage_info()
    {
        clearos_profile(__METHOD__, __LINE__);

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();
        $retval = $shell->execute(self::COMMAND_CURL, $this->common_cmd_call.' "'.self::HOST.'/host/storage"', TRUE, $options);
        $lines = $shell->get_output();

        $error = (preg_match('/Failed connect/', $lines[2])) ? lang('sia_app_connection_error') : NULL;
        
        if ($error) {
            throw new Engine_Exception($error);
        
        } else {
            return $this->_filter_response($lines, 3);
        }
    }

    /**
     * Fetches status information about the host.
     *
     * @return @array Array of available host setting
     */

    public function get_hosting_settings()
    {

        clearos_profile(__METHOD__, __LINE__);

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();
        $retval = $shell->execute(self::COMMAND_CURL, $this->common_cmd_call.' "'.self::HOST.'/host"', TRUE, $options);
        $lines = $shell->get_output();

        $error = (preg_match('/Failed connect/', $lines[2])) ? lang('sia_app_connection_error') : NULL;
        
        if ($error) {
            throw new Engine_Exception($error);
        
        } else {
            return $this->_filter_response($lines, 3);
        }
    }

    /**
    * Configures hosting parameter.
    *
    * @param @string  $max_duration Hosting duration
    * @param @string  $collateral   Hosting collateral
    * @param @string  $price_per_tb Hosting price_per_tb
    * @param @string  $bandwidth    Hosting bandwidth
    * @param @boolean $accepting    Hosting accepting
    *
    * @return @array Array of available host setting
    *
    */

    public function set_hosting_settings($max_duration, $collateral, $price_per_tb, $bandwidth, $accepting)
    {
        clearos_profile(__METHOD__, __LINE__);

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();
        
        $retval = $shell->execute(self::COMMAND_CURL, '-A "Sia-Agent" --user "":"'.$this->apipassword.'" --data "maxduration='.$max_duration.'&collateral='.$collateral.'&minstorageprice='.$bandwidth.'&mindownloadbandwidthprice='.$price_per_tb.'&acceptingcontracts='.$accepting.'" "'.self::HOST.'/host"', TRUE, $options);
        
        $lines = $shell->get_output();
        $error = (preg_match('/wallet must be unlocked/', $lines[3])) ? lang('sia_app_wallet_unlocked') : NULL;
        $error = (preg_match('/Refer to API/', $lines[3])) ? lang('sia_app_404_refer_api') : NULL;

        if ($error) {
            throw new Engine_Exception($error);

        } elseif ($error1) {
            throw new Engine_Exception($error1);

        } else {
            return $this->_filter_response($lines, 3);
        }
    }

    /**
    * Adds a storage folder to the manager.
    *
    * @param @string $storage_path        storage path of folder
    * @param @string $storage_folder_size folder size of folder
    *
    * @return @string standard success or error response.
    */

    public function add_storage_folder($storage_path, $storage_folder_size)
    {
        clearos_profile(__METHOD__, __LINE__);

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $size = $storage_folder_size * 1024 * 1024 * 1024;

        $shell = new Shell();
        $retval = $shell->execute(self::COMMAND_CURL, '-A "Sia-Agent" --user "":"'.$this->apipassword.'" --data "path='.$storage_path.'&size='.$size.'" "'.self::HOST.'/host/storage/folders/add"', TRUE, $options);
        $lines = $shell->get_output();
        $error = (preg_match('/minimum allowed size/', $lines[3])) ? lang('sia_app_allowed_size') : NULL;
        $error2 = (preg_match('/selected path is already/', $lines[3])) ? lang('sia_app_path_is_already') : NULL;
        $error3 = (preg_match('/no such file/', $lines[3])) ? lang('sia_app_no_such_file') : NULL;
        $error4 = (preg_match('/must be absolute/', $lines[3])) ? lang('sia_app_must_absolute') : NULL;
        $error5 = (preg_match('/Refer to API/', $lines[3])) ? lang('sia_app_404_refer_api') : NULL;

        if ($error) {
            throw new Engine_Exception($error);

        } elseif ($error2) {
            throw new Engine_Exception($error2);
        
        } elseif ($error3) {
            throw new Engine_Exception($error3);
    
        } elseif ($error4) {
            throw new Engine_Exception($error4);

        } elseif ($error5) {
            throw new Engine_Exception($error5);

        } else {
            return $this->_filter_response($lines, 3);
        }
    }

    /**
     * Locks the wallet, wiping all secret keys.
     *
     * @return @string standard success or error response.
     */

    public function wallet_lock()
    {
        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();
        $retval = $shell->execute(self::COMMAND_CURL, '-A "Sia-Agent" --user "":"'.$this->apipassword.'" --data "" "'.self::HOST.'/wallet/lock"', TRUE, $options);
        
        $lines = $shell->get_output();

        // Create lock file when user locks the wallet
        //---------------

        try {
            $file = new File(self::FILE_USER_UNLOCK, TRUE);

            if ($file->exists()) {
                $file->delete();
            }

        } catch (Exception $e) {
           throw new Engine_Exception($e);
        }

        return $this->_filter_response($lines, 3);
    }

    /**
     * Check is user made an unlock.
     *
     * @return @boolean
     */

    public function check_user_unlock_exists()
    {
        $file = new File(self::FILE_USER_UNLOCK, TRUE);

        return $file->exists();
    }

    /**
     * Unlocks the wallet. The wallet is capable of knowing whether the correct password was provided.
     *
     * @param @string $password password of wallet unlock
     *
     * @return @string standard success or error response.
     */

    public function wallet_unlock($password)
    {
        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();
        $retval = $shell->execute(self::COMMAND_CURL, '-m 1 -A "Sia-Agent" --user "":"'.$this->apipassword.'"  --data "encryptionpassword='.$password.'" "'.self::HOST.'/wallet/unlock"', TRUE, $options);
        
        $lines = $shell->get_output();
        $error = (preg_match('/authentication failed/', $lines[3])) ? lang('sia_app_authentication') : NULL;
        $error2 = (preg_match('/encryption key is incorrect/', $lines[3])) ? lang('sia_app_key_is_incorrect') : NULL;
        
        if ($error) {
            throw new Engine_Exception($error);

        } elseif ($error2) {
            throw new Engine_Exception($error2);
        
        } else {
            $data = $this->_filter_response($lines, 3);
        }
        
        //create user unlock file
        $file = new File(self::FILE_USER_UNLOCK, TRUE);

        if (!$file->exists()) {
            $file->create('root', 'root', "0755");
        }

        return $data;
    }

    /**
     * Set a flag when Wallet is locked / this function is for daemon only.
     *
     *
     * @return @boolean TRUE.
     */

    public function mark_wallet_is_locked()
    {
        $file = new File(self::FILE_USER_UNLOCK, TRUE);

        if ($file->exists()) {
            $file->delete();
        }

        return TRUE;
    }

    /**
     * Fetches the list of addresses from the wallet.
     *
     * @return Array of wallet addresses owned by the wallet.
     */

    public function get_wallet_address()
    {
        clearos_profile(__METHOD__, __LINE__);

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();
        $retval = $shell->execute(self::COMMAND_CURL, $this->common_cmd_call.' "'.self::HOST.'/wallet/addresses"', TRUE, $options);
        $lines = $shell->get_output();
        
        return $this->_filter_response($lines, 3);
    }

    /**
     * Gets a new address from the wallet generated by the primary seed.
     *
     *@return @string hash Wallet address
     */

    public function create_wallet_address()
    {
        clearos_profile(__METHOD__, __LINE__);

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();
        $retval = $shell->execute(self::COMMAND_CURL, $this->common_cmd_call.' "'.self::HOST.'/wallet/address"', TRUE, $options);
        
        $lines = $shell->get_output();
        $error = (preg_match('/must be unlocked/', $lines[3])) ? lang('sia_app_wallet_unlocked') : NULL;

        if ($error) {
            throw new Engine_Exception($error);
        
        } else {
            return $this->_filter_response($lines, 3);
        }
    }

    /**
    * Remove a storage folder from the manager.
    *
    * @param @string $hostpath host path of folder
    *
    * @return standard success or error response
    */

    public function remove_host_folder($hostpath)
    {
        clearos_profile(__METHOD__, __LINE__);

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();
        $retval = $shell->execute(self::COMMAND_CURL, '-A "Sia-Agent" --user "":"'.$this->apipassword.'" --data "path='.$hostpath.'" "'.self::HOST.'/host/storage/folders/remove"', TRUE, $options);
        $lines = $shell->get_output();
        $error = (preg_match('/no storage folder exists/', $lines[3])) ? lang('sia_app_folder_exists') : NULL;

        if ($error) {
            throw new Engine_Exception($error);
        
        } else {
            return $this->_filter_response($lines, 3);
        }
    }

    /**
     * Grows or shrinks a storage file in the manager.
     *
     * @param @string $storage_path        path of folder
     *
     * @param @string $storage_folder_size folder size(bytes)
     *
     * @return @string standard success or error response
     */

    public function resize_host_folder($storage_path, $storage_folder_size)
    {
        clearos_profile(__METHOD__, __LINE__);

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';
        $size = $storage_folder_size * 1024 * 1024 * 1024;

        $shell = new Shell();
        $retval = $shell->execute(self::COMMAND_CURL, '-A "Sia-Agent" --user "":"'.$this->apipassword.'" --data "path='.$storage_path.'&newsize='.$size.'" "'.self::HOST.'/host/storage/folders/resize"', TRUE, $options);
        
        $lines = $shell->get_output();

        $error = (preg_match('/minimum allowed size/', $lines[3])) ? lang('sia_app_allowed_size') : NULL;
        $error2 = (preg_match('/selected path is already/', $lines[3])) ? lang('sia_app_path_is_already') : NULL;
        $error3 = (preg_match('/no such file/', $lines[3])) ? lang('sia_app_no_such_file') : NULL;
        $error4 = (preg_match('/must be absolute/', $lines[3])) ? lang('sia_app_must_absolute') : NULL;
        $error5 = (preg_match('/could not find storage/', $lines[3])) ? lang('sia_app_not_find_storage') : NULL;

        if ($error) {
            throw new Engine_Exception($error);

        } elseif ($error2) {
            throw new Engine_Exception($error2);
        
        } elseif ($error3) {
            throw new Engine_Exception($error3);
    
        } elseif ($error4) {
            throw new Engine_Exception($error4);
        
        } elseif ($error5) {
            throw new Engine_Exception($error5);
        
        } else {
            return $this->_filter_response($lines, 3);
        }
    }

    /**
     * Initializes the wallet.
     *
     * @param @string $encryptionpassword encryptionpassword of wallet unlock
     *
     * @return @string Wallet seed used to generate addresses that the wallet is able to spend.
     */

    public function wallet_initializing($encryptionpassword)
    {
        clearos_profile(__METHOD__, __LINE__);

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';
        $shell = new Shell();

        $retval = $shell->execute(self::COMMAND_CURL, '-A "Sia-Agent" --user "":"'.$this->apipassword.'" --data "encryptionpassword='.$encryptionpassword.'" "127.0.0.1:9980/wallet/init"', TRUE, $options);
        $lines = $shell->get_output();

        $error = (preg_match('/authentication failed/', $lines[3])) ? lang('sia_app_authentication') : NULL;
        $error2 = (preg_match('/encryption key is incorrect/', $lines[3])) ? lang('sia_app_key_is_incorrect') : NULL;
        $error3 = (preg_match('/cannot encrypt again/', $lines[3])) ? lang('sia_app_encrypt_again') : NULL;
        
        if ($error) {
            throw new Engine_Exception($error);

        } elseif ($error2) {
            throw new Engine_Exception($error2);
        
        } elseif ($error3) {
            throw new Engine_Exception($error3);
        
        } else {
            return $this->_filter_response($lines, 3);
        }
    }

    /**
     * Returns the list of seeds in use by the wallet.
     *
     * @return @array Returns the list of seeds in use by the wallet.
     */

    public function get_wallet_seeds()
    {
        clearos_profile(__METHOD__, __LINE__);

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();
        $retval = $shell->execute(self::COMMAND_CURL, $this->common_cmd_call.' "'.self::HOST.'/wallet/seeds"', TRUE, $options);
        
        $lines = $shell->get_output();
    
        return $this->_filter_response($lines, 3);
    }

    /**
     * Announce the host to the network as a source of storage. Generally only needs to be called once.
     *
     * @param @string $address_announce for host address
     *
     * @return @string standard success or error response
     */

    public function host_announce($address_announce)
    {
        clearos_profile(__METHOD__, __LINE__);

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();
        $retval = $shell->execute(self::COMMAND_CURL, $this->common_cmd_call.' --data "netaddress='.$address_announce.'" "'.self::HOST.'/host/announce"', TRUE, $options);
        
        $lines = $shell->get_output();
        
        $error = (preg_match('/requested with local net address/', $lines[3])) ? lang('sia_app_local_net_address') : NULL;

        $error2 = (preg_match('/insufficient/', $lines[3])) ? lang('sia_app_insufficient') : NULL;

        $error3 = (preg_match('/missing port in address/', $lines[3])) ? lang('sia_app_port_in_address') : NULL;

        $error4 = (preg_match('/wallet is locked/', $lines[3])) ? lang('sia_app_wallet_is_locked') : NULL;

        if ($error) {
            throw new Engine_Exception($error);

        } elseif ($error2) {
            throw new Engine_Exception($error2);

        } elseif ($error3) {
            throw new Engine_Exception($error3);
        
        } elseif ($error4) {
            throw new Engine_Exception($error4);

        } else {
            return $this->_filter_response($lines, 3);
        }
    }

     /**
     * Sends siafunds to an address.
     *
     * @param @string $sia_siafunds    siafunds
     * @param @string $sia_destination destination of sia fund
     *
     * @return @string standard success or error response
     */

    public function send_sia_funds($sia_siafunds, $sia_destination)
    {
        clearos_profile(__METHOD__, __LINE__);

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';
      

        $shell = new Shell();
        $retval = $shell->execute(self::COMMAND_CURL, '-A "Sia-Agent" --user "":"'.$this->apipassword.'" --data "amount='.$sia_siafunds.'&destination='.$sia_destination.'" "'.self::HOST.'/wallet/siafunds"', TRUE, $options);
        
        $lines = $shell->get_output();

        $error = (preg_match('/wallet must be unlocked/', $lines[3])) ? lang('sia_app_wallet_unlocked') : NULL;
        $error2 = (preg_match('/insufficient/', $lines[3])) ? lang('sia_app_insufficient') : NULL;
    
        if ($error) {
            throw new Engine_Exception($error);

        } elseif ($error2) {
            throw new Engine_Exception($error2);
        } else {
            return $this->_filter_response($lines, 3);
        }

    }

    /**
    * Changes the wallet's encryption key.
    *
    * @param @string $encryptionpassword old encryptionpassword
    * @param @string $newpassword        new password
    *
    * @return @string standard success or error response
    */

    public function change_wallet_password($encryptionpassword, $newpassword)
    {
        clearos_profile(__METHOD__, __LINE__);

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';
      

        $shell = new Shell();
        $retval = $shell->execute(self::COMMAND_CURL, '-A "Sia-Agent" --user "":"'.$this->apipassword.'" --data "encryptionpassword='.$encryptionpassword.'&newpassword='.$newpassword.'" "'.self::HOST.'/wallet/changepassword"', TRUE, $options);
        
        $lines = $shell->get_output();

        $error = (preg_match('/password must be provided/', $lines[3])) ? lang('sia_app_password_must') : NULL;
        $error2 = (preg_match('/encryption key is incorrect/', $lines[3])) ? lang('sia_app_key_is_incorrect') : NULL;
    
        if ($error) {
            throw new Engine_Exception($error);

        } elseif ($error2) {
            throw new Engine_Exception($error2);

        } else {
            return $this->_filter_response($lines, 3);
        }

    }

    ///////////////////////////////////////////////////////////////////////////////
    // V A L I D A T I O N   R O U T I N E S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Validates password.
     *
     * @param string $password password
     *
     * @return string error message if encryption password is invalid
     */

    public function validate_encryptionpassword($password)
    {
        clearos_profile(__METHOD__, __LINE__);

        return;
    }

    /**
    * Validates password.
    *
    * @param string $password password
    *
    * @return string error message if password is invalid
    */

    public function validate_password($password)
    {
        clearos_profile(__METHOD__, __LINE__);

        if (strlen($password) == 0 || strlen($password) < self::MIN_PASSWORD_LENGTH)
            return lang('password_is_too_short');

    }

    /**
     * Validation for integer value.
     *
     * @param string $value integer value.
     *
     * @return string error message if host setting is invalid
     */

    public function validate_integer_value($value)
    {
        clearos_profile(__METHOD__, __LINE__);

        return;
    }

    /**
    * Validation for Accepting Contracts.
    *
    * @param string $value boolean value.
    *
    * @return string error message if accepting is no
    */

    public function validate_accepting($value)
    {
        clearos_profile(__METHOD__, __LINE__);

        return;
    }

    /**
    * Validation for Host.
    *
    * @param string $value string value.
    *
    * @return string error message if host is invalid
    */

    public function validate_host($value)
    {
        
        clearos_profile(__METHOD__, __LINE__);

        return;
    }

    /**
    * Validation for Host.
    *
    * @param string $value string value.
    *
    * @return string error message if storage path is invalid
    */

    public function validate_storage_path($value)
    {
        clearos_profile(__METHOD__, __LINE__);

       
        return;
    }

    /**
    * Validation for folder size.
    *
    * @param string $value string value.
    *
    * @return string error message if folder size is invalid
    */

    public function validate_folder_size($value)
    {
        clearos_profile(__METHOD__, __LINE__);

       
        return;
    }

    /**
    * Validation for sia funds.
    *
    * @param string $value string value.
    *
    * @return string error message if siafunds is invalid
    */

    public function validate_siafunds($value)
    {
        clearos_profile(__METHOD__, __LINE__);
        return;
    }

    /**
    * Get SIA Wallet Setting
    *
    *@return @string Wallet Setting
    */

    public function get_wallet_setting()
    {

        clearos_profile(__METHOD__, __LINE__);

        //create file for wallet settings
        //...................................

        $file = new File(self::WALLET_SETTINGS_FILE, TRUE);

        if (!$file->exists()) {
            $file->create('root', 'root', "0777");
        }

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();

        $retval = $shell->execute(self::COMMAND_CURL, '-A "Sia-Agent" -u "":"'.$this->apipassword.'" "'.self::HOST.'/wallet/backup?destination='.self::WALLET_SETTINGS_FILE.'"', TRUE, $options);
        $lines = $shell->get_output();
        
        return $lines;
    }

    ///////////////////////////////////////////////////////////////////////////////
    // P R O T E C T E D   M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Get SIA api password
     *
     * @return @string api password
     */

    protected function _get_sia_apipassword()
    {
        clearos_profile(__METHOD__, __LINE__);

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';
        $shell = new Shell();

        try {
            $retval = $shell->execute(self::COMMAND_LOCATE, " /var/lib/docker/ -name '.sia'", TRUE, $options);
        
            $lines  = $shell->get_output();
            $folder = $lines[0];
            $path   = $folder .'/apipassword';
            
            $file = new File($path, TRUE);
            $apipassword = $file->get_contents($path);
            return trim($apipassword);

        } catch (Exception $e) {
           throw new Exception($e->getMessage());
        }
    }

    /**
    * Get filtered response from APIs output
    *
    * @param @json  $response output commands
    * @param @array $index    index of array
    *
    * @return @array response
    */

    protected function _filter_response($response, $index =3)
    {
        $data = json_decode($response[$index], TRUE);
        return $data;
    }
}