<?php

/**
 * Storage class.
 *
 * Class to assist in the discovery of mass sia devices on the server.
 *
 * @category   apps
 * @package    sia
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/sia/
 */
///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\sia;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('sia_lang');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

use \clearos\apps\base\Engine as Engine;
use \clearos\apps\base\Folder as Folder;

use \Exception as Exception;
use \clearos\apps\base\Engine_Exception as Engine_Exception;
use \clearos\apps\base\Validation_Exception as Validation_Exception;

clearos_load_library('base/Engine');
clearos_load_library('base/Folder');

clearos_load_library('base/Engine_Exception');
clearos_load_library('base/Validation_Exception');
///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Storage class.
 *
 * @category   apps
 * @package    sia
 * @subpackage libraries
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2019 ClearCenter
 * @license    http://www.clearcenter.com/app_license ClearCenter license
 * @link       http://www.clearcenter.com/support/documentation/clearos/clearglass-community/
 */

class Host_Storage extends Engine
{

    ///////////////////////////////////////////////////////////////////////////////
    // M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Storage constructor.
     */

    public function __construct()
    {
        clearos_profile(__METHOD__, __LINE__);
    }

    /**
     * Get list from the given folder.
     * @param @string $path for folder path
     * @return @array list of folder
     */

    public function get_folder_list($path ="/")
    {
        clearos_profile(__METHOD__, __LINE__);

        $folder = new Folder($path, FALSE);

        if (!$folder->is_directory()) {
            return FALSE;
        }

        $listing = $folder->get_listing(TRUE, FALSE);
        return $listing;
    }
}