<?php

clearos_load_language('sia');

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'sia';
$app['version'] = '1.0.7';
$app['vendor'] = 'ClearFoundation';
$app['packager'] = 'ClearFoundation';
$app['license'] = 'GPLv3';
$app['license_core'] = 'LGPLv3';
$app['description'] = lang('sia_app_description');

$app['powered_by'] = array(
    'vendor' => array(
        'name' => 'Sia',
        'url' => 'https://www.sia.tech/',
    ),
    'packages' => array(
        'sia' => array(
            'name' => 'Sia',
            'version' => '---',
        ),
    ),
);


/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('sia_app_name');
$app['category'] = lang('base_category_server');
$app['subcategory'] = lang('base_subcategory_file');

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

$app['requires'] = array(
    'app-docker >= 2.6.1',
);


$app['core_requires'] = array(
    'curl',
    'app-docker-core',
);

$app['core_directory_manifest'] = array(
    '/var/clearos/sia' => array(),
    '/var/lib/sia' => array(),
    '/var/clearos/docker/project' => array(),
);

$app['core_file_manifest'] = array(
    'sia.php'=> array('target' => '/var/clearos/docker/project/sia.php'),
    'docker-compose.yml'=> array('target' => '/var/lib/sia/docker-compose.yml'),
);
