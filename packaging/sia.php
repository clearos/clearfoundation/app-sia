<?php

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// C O N F I G L E T
///////////////////////////////////////////////////////////////////////////////

$configlet = array(
    'title' => "SIA",
    'app_name' => 'sia',
    'base_project' => 'sia',
    'container_count' => 1,
    'docker_compose_file' => '/var/lib/sia/docker-compose.yml',
    'images' => [
        'mtlynch/docker-sia:latest' => 'sia',
    ]
);
