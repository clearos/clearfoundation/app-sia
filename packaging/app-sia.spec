
Name: app-sia
Epoch: 1
Version: 1.0.7
Release: 1%{dist}
Summary: Sia Manager
License: GPLv3
Group: Applications/Apps
Packager: ClearFoundation
Vendor: ClearFoundation
Source: %{name}-%{version}.tar.gz
Buildarch: noarch
Requires: %{name}-core = 1:%{version}-%{release}
Requires: app-base
Requires: app-docker >= 2.6.1

%description
Sia is the leading decentralized cloud storage platform.

%package core
Summary: Sia Manager - API
License: LGPLv3
Group: Applications/API
Requires: app-base-core
Requires: curl
Requires: app-docker-core

%description core
Sia is the leading decentralized cloud storage platform.

This package provides the core API and libraries.

%prep
%setup -q
%build

%install
mkdir -p -m 755 %{buildroot}/usr/clearos/apps/sia
cp -r * %{buildroot}/usr/clearos/apps/sia/
rm -f %{buildroot}/usr/clearos/apps/sia/README.md

install -d -m 0755 %{buildroot}/var/clearos/docker/project
install -d -m 0755 %{buildroot}/var/clearos/sia
install -d -m 0755 %{buildroot}/var/lib/sia
install -D -m 0644 packaging/docker-compose.yml %{buildroot}/var/lib/sia/docker-compose.yml
install -D -m 0644 packaging/sia.php %{buildroot}/var/clearos/docker/project/sia.php

%post
logger -p local6.notice -t installer 'app-sia - installing'

%post core
logger -p local6.notice -t installer 'app-sia-api - installing'

if [ $1 -eq 1 ]; then
    [ -x /usr/clearos/apps/sia/deploy/install ] && /usr/clearos/apps/sia/deploy/install
fi

[ -x /usr/clearos/apps/sia/deploy/upgrade ] && /usr/clearos/apps/sia/deploy/upgrade

exit 0

%preun
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-sia - uninstalling'
fi

%preun core
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-sia-api - uninstalling'
    [ -x /usr/clearos/apps/sia/deploy/uninstall ] && /usr/clearos/apps/sia/deploy/uninstall
fi

exit 0

%files
%defattr(-,root,root)
/usr/clearos/apps/sia/controllers
/usr/clearos/apps/sia/htdocs
/usr/clearos/apps/sia/views

%files core
%defattr(-,root,root)
%doc README.md
%exclude /usr/clearos/apps/sia/packaging
%exclude /usr/clearos/apps/sia/unify.json
%dir /usr/clearos/apps/sia
%dir /var/clearos/docker/project
%dir /var/clearos/sia
%dir /var/lib/sia
/usr/clearos/apps/sia/deploy
/usr/clearos/apps/sia/language
/usr/clearos/apps/sia/libraries
/var/lib/sia/docker-compose.yml
/var/clearos/docker/project/sia.php
