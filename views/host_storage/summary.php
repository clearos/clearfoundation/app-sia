<?php

/**
 * Sia summary view.
 *
 * @category   apps
 * @package    sia
 * @subpackage views
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/sia/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('sia');

///////////////////////////////////////////////////////////////////////////////
// Headers
///////////////////////////////////////////////////////////////////////////////

$headers = array(lang('sia_app_capacity'),lang('sia_app_add_capacity_remaining'),lang('sia_app_path'),lang('sia_app_progress'));
$tb_title = lang('sia_app_hosts_manager');

///////////////////////////////////////////////////////////////////////////////
// Anchors 
///////////////////////////////////////////////////////////////////////////////

if ($wallet_lock_status) {

    $anchors = array(anchor_custom('/app/sia/host_storage/add_folder', lang('sia_app_add_folder')));
    
} else {

    $anchors = array();
}

///////////////////////////////////////////////////////////////////////////////
// Items
///////////////////////////////////////////////////////////////////////////////


foreach ($host_storage as $key => $values) {

    foreach ($values as $key => $value) {

            $host_path = strtr(base64_encode($value['path']),  '+/=', '-_.');
            $capacity = number_format($value['capacity'] /1073741824 , 2). ' GB';
            $remaining = number_format($value['capacityremaining'] /1073741824 , 2). ' GB';
            $item1['title'] = 'title';
            $item1['action'] = '';
            $item1['anchors'] = button_set(
                array(
                    anchor_custom('/app/sia/host_storage/resize_folder/'.$host_path, lang('sia_app_folder_resize')),
                    anchor_custom('/app/sia/host_storage/remove_folder/'.$host_path, lang('sia_app_folder_delete'))
                )
            );
            $item1['details'] = array($capacity,$remaining,$value['path'],$value['ProgressNumerator']);

            $items1[] = $item1;
    }

}

sort($items1);

///////////////////////////////////////////////////////////////////////////////
// Hosts Storage Manager
///////////////////////////////////////////////////////////////////////////////

$options = array(
    'id' => 'storage_informations',
    //'responsive' => array(1 => 'none')
);
echo summary_table(
    $tb_title,
    $anchors,
    $headers,
    $items1,
    $options
);

