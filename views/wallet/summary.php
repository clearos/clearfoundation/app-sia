<?php

/**
 * Wallet summary view.
 *
 * @category   apps
 * @package    sia
 * @subpackage views
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/sia/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////
?>
<style>
    .theme-loading-normal{ background: rgba(0, 0, 0, 0.4); color:#fff; position: absolute;
       top: 0; right: 0; bottom: 0; left: 0; z-index: 100; text-align: center;}
    .theme-loading-normal span{ display: block; width: 100%; font-size: 16px; font-weight: 600; }
    .theme-loading-normal span.loadeicon{ display: block; width: 100%; clear: both; padding-top: 20px;}
    
    .load_table{ display: table; width: 100%; height: 100%; }
    .load_cell{ display: table-cell; width: 100%; height: 100%; vertical-align: middle; padding: 20px; }

    .load_cell .loader { display: block; margin: 0 auto;
  border: 2px solid rgba(255, 255, 255, 0.1);
  border-radius: 50%;
  border-top: 2px solid #fff;
  width: 30px;
  height: 30px;
  -webkit-animation: spin 2s linear infinite; 
  animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

</style>
<?php 

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('sia');

echo infobox_info('Help', lang('sia_app_help'));
echo '<input type="hidden" id="show_loader" value="'.$show_loader.'">';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// My Account Table Headers
///////////////////////////////////////////////////////////////////////////////

$headers = array(lang('sia_app_unlocked'), lang('sia_app_height'));
$blockheight = lang('sia_app_my_account');

if ($wallet_lock_status) {
  
    $unlocked = '<label class="label label-success"> Unlocked </label>';

} else { 
    $unlocked = '<label class="label label-danger"> Locked </label>';

}

///////////////////////////////////////////////////////////////////////////////
// Anchors 
///////////////////////////////////////////////////////////////////////////////

$encrypted = $block['encrypted'];

if ($wallet_lock_status) {
    $anchors = array(
      anchor_custom('/app/sia/wallet/get_seeds', lang('sia_seed_anchor')),
      anchor_custom('/app/sia/wallet/wallet_lock', lang('sia_app_wallet_lock')),
      anchor_custom('/app/sia/wallet/change_password', lang('sia_change_password_anchor'))
    );
     
} else { 

    $options = array("id" => 'walletunlock');
  
    if ($encrypted) {
        $anchors = array(
         anchor_custom('/app/sia/wallet/wallet_unlock', lang('sia_app_unlock'), '', $options)
        );

    } else {
        $anchors = array( anchor_custom('/app/sia/wallet/initializes', lang('sia_app_create_account')));
    }
}

if ($wallet_lock_status) {

    $buttons_set = button_set(array(anchor_custom('/app/sia/host_setting/announce', lang('sia_app_announce'))));

} else {
    $buttons_set = button_set(array());
}

///////////////////////////////////////////////////////////////////////////////
// Items
///////////////////////////////////////////////////////////////////////////////

//foreach ($block as $key => $values) {

    $item['title'] = 'title';
    $item['action'] = '';
    $item['anchors'] = $buttons_set;
    $item['details'] = array($unlocked, $block['height']);

    $items[] = $item;
//}

sort($items);

///////////////////////////////////////////////////////////////////////////////
// My acccount Summary table
///////////////////////////////////////////////////////////////////////////////

$options = array(
    'id' => 'block_heights',
    //'responsive' => array(1 => 'none')
);
echo summary_table(
    $blockheight,
    $anchors,
    $headers,
    $items,
    $options
);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//My sia funds Headers
///////////////////////////////////////////////////////////////////////////////

$headers = array(lang('sia_app_confirmed'), lang('sia_app_balance'));
$blockheight = lang('sia_app_my_sia_funds');

///////////////////////////////////////////////////////////////////////////////
// Anchors 
///////////////////////////////////////////////////////////////////////////////

$anchors = array();

if ($wallet_lock_status) {
    $buttons_set = button_set(
        array(
          anchor_custom('https://sia.tech/get-siacoin', lang('sia_app_buy_sia'), '', array("id"=> "buy_sia")),
          anchor_custom('/app/sia/wallet/siafunds', lang('sia_app_send_sia'))
        )
    );

} else {

    $buttons_set = button_set(
        array(
          anchor_custom('https://sia.tech/get-siacoin', lang('sia_app_buy_sia'), '', array("id"=> "buy_sia"))
        )
    );
}

///////////////////////////////////////////////////////////////////////////////
// Items
///////////////////////////////////////////////////////////////////////////////

//foreach ($block as $key => $values) {

    $itemsia['title'] = 'title';
    $itemsia['action'] = '';
    $itemsia['anchors'] = $buttons_set;
    $itemsia['details'] = array($block['confirmedsiacoinbalance'].' SC', $block['siafundbalance'].' SC');

    $itemsias[] = $itemsia;
//}

sort($itemsias);

///////////////////////////////////////////////////////////////////////////////
// My acccount Summary table
///////////////////////////////////////////////////////////////////////////////

$options = array(
    'id' => 'my_sia_funds',
    //'responsive' => array(1 => 'none')
);
echo summary_table(
    $blockheight,
    $anchors,
    $headers,
    $itemsias,
    $options
);


///////////////////////////////////////////////////////////////////////////////
//Wallet addresses  Headers
///////////////////////////////////////////////////////////////////////////////

$headers = array(lang('sia_app_wallet_address'), lang('sia_app_wallet_address'));
$tb_title = lang('sia_app_wallet_address_title');

///////////////////////////////////////////////////////////////////////////////
// Anchors 
///////////////////////////////////////////////////////////////////////////////

if ($wallet_lock_status) {
    $anchors = array(anchor_custom('/app/sia/wallet/get_address', lang('sia_app_wallet_create_addresses')));

} else {
    $anchors = array();
}

///////////////////////////////////////////////////////////////////////////////
// Items
///////////////////////////////////////////////////////////////////////////////


foreach ($wallet_address as $key => $values) {

    foreach ($values as $key => $value) {

            $item2['title'] = 'title';
            $item2['action'] = '';
            $item2['anchors'] = button_set();
            $item2['details'] = array(substr($value, 0, 55) . "...", $value);

            $items2[] = $item2;
    }
}

sort($items2);

///////////////////////////////////////////////////////////////////////////////
//Wallet addresses  Summary table
///////////////////////////////////////////////////////////////////////////////

$options = array(
    'id' => 'wallet_addressesss',
    //'responsive' => array(1 => 'none')
);
echo summary_table(
    $tb_title,
    $anchors,
    $headers,
    $items2,
    $options
);

