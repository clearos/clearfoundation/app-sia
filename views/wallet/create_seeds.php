<?php

/**
 * sia summary view.
 *
 * @category   apps
 * @package    sia
 * @subpackage views
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/sia/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('sia');


echo infobox_warning('Warning', lang('sia_warning_seeds'));

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Headers
///////////////////////////////////////////////////////////////////////////////

$headers = array(lang('sia_app_primary_seed'));
$title = lang('sia_app_primary_seed');

///////////////////////////////////////////////////////////////////////////////
// Anchors 
///////////////////////////////////////////////////////////////////////////////

$anchors = array(anchor_custom('/app/sia', lang('sia_app_return_summary')));

///////////////////////////////////////////////////////////////////////////////
// Items
///////////////////////////////////////////////////////////////////////////////

//foreach ($seeds['allseeds'] as $key => $values) {
    $options_data = array("id" => 'sia_seeds');
    $keys = $seeds['primaryseed'];

    $item['title'] = 'title';
    $item['action'] = '';
    $item['anchors'] = button_set(array(anchor_custom("data:text/plain;charset=UTF-8,$keys", 'Download' , '', $options_data)));
    $item['details'] = array($seeds['primaryseed']);

    $items[] = $item;
//}

sort($items);


///////////////////////////////////////////////////////////////////////////////
// Summary table
///////////////////////////////////////////////////////////////////////////////

$options = array(
    'id' => 'seeds',
    //'responsive' => array(1 => 'none')
);
echo summary_table(
    $title,
    $anchors,
    $headers,
    $items,
    $options
);

