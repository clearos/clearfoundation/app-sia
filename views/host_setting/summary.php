<?php

/**
 * Host setting summary view.
 *
 * @category   apps
 * @package    sia
 * @subpackage views
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/sia/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('sia');

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Get contract information from the host database. lang('sia_app_confirmed')
///////////////////////////////////////////////////////////////////////////////

$headers = array(lang('sia_contract_host'), lang('sia_contract_active'), lang('sia_contract_earned'), lang('sia_contract_expected'));
$title = lang('sia_contract_title');

///////////////////////////////////////////////////////////////////////////////
// Anchors 
///////////////////////////////////////////////////////////////////////////////

 $anchors = array();

///////////////////////////////////////////////////////////////////////////////
// Items
///////////////////////////////////////////////////////////////////////////////

 $connectabilitystatus = $hostingsetting['connectabilitystatus'];
 $contractcount = $hostingsetting['financialmetrics']['contractcount'];
 $storagerevenue = $hostingsetting['financialmetrics']['storagerevenue'];
 $downloadbandwidthrevenue = $hostingsetting['financialmetrics']['downloadbandwidthrevenue'];

//foreach ($bloks as $key => $values) {

    $itemsia['title'] = 'title';
    $itemsia['action'] = '';
    $itemsia['anchors'] = button_set(array());
    $itemsia['details'] = array(ucfirst($connectabilitystatus), $contractcount, $storagerevenue .' SC', $downloadbandwidthrevenue.' SC');

    $itemsias[] = $itemsia;
//}

sort($itemsias);

///////////////////////////////////////////////////////////////////////////////
//Contractcount Summary table
///////////////////////////////////////////////////////////////////////////////

$options = array(
    'id' => 'my_sia_funds',
    //'responsive' => array(1 => 'none')
);
echo summary_table(
    $title,
    $anchors,
    $headers,
    $itemsias,
    $options
);




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Host setting Summary table
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// Form
///////////////////////////////////////////////////////////////////////////////

 $maxduration = $hostingsetting['externalsettings']['maxduration'];
 $collateral = $hostingsetting['externalsettings']['collateral'];
 $acceptingcontracts = $hostingsetting['externalsettings']['acceptingcontracts'];
 $downloadbandwidthprice = $hostingsetting['internalsettings']['mindownloadbandwidthprice'];
 $contractprice = $hostingsetting['internalsettings']['minstorageprice'];


echo form_open('sia/host_setting');
echo form_header(lang('sia_app_host_settings'));
echo field_checkbox('accepting', $acceptingcontracts, lang('sia_app_folder_Accepting'), FALSE);
echo field_input('max_duration', $maxduration, lang('sia_app_max_duration'), FALSE);
echo field_input('collateral', $collateral, lang('sia_app_collateral'), FALSE);
echo field_input('price_per_tb',$downloadbandwidthprice, lang('sia_app_price'), FALSE);
echo field_input('bandwidth', $contractprice, lang('sia_app_folder_bandwidth'), FALSE);
echo field_button_set(
    array(form_submit_custom('submit',lang('sia_app_folder_save')))
);

echo form_footer();
echo form_close();